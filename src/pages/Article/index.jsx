import React from "react";
import { connect } from "react-redux";

import { testExample } from "../../store/example";

const Article = ({ example, testExample }) => (
  <div>
    <span onClick={testExample}>Teste</span>
    {example}
  </div>
);

const mapStateToProps = state => ({
  example: state.example.example
});

const mapDispatchToProps = {
  testExample
};

export default connect(mapStateToProps, mapDispatchToProps)(Article);
