import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import exampleReducer from "./example";

export const Reducers = combineReducers({
  example: exampleReducer
});

export const Store = createStore(Reducers, applyMiddleware(thunk));
