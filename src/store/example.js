const SET_EXAMPLE = "SET_SET_EXAMPLE";

export const testExample = () => {
  return dispatch => {
    dispatch(setExample(" testado"));
  };
};

export const setExample = example => ({
  type: SET_EXAMPLE,
  payload: example
});

const initialState = {
  example: "...",
  requests: {
    example: false
  }
};

const reducer = {
  [SET_EXAMPLE]: (stt, payload) => ({
    ...stt,
    example: payload
  })
};

export default function exampleReducer(
  state = initialState,
  { type, payload }
) {
  if (typeof reducer[type] === "function") {
    return reducer[type](state, payload);
  }

  return state;
}
