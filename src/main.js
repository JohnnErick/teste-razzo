import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";

import { Store } from "./store";
import Article from "./pages/Article";

const App = () => (
  <Provider store={Store}>
    <Article />
  </Provider>
);

ReactDOM.render(<App />, document.querySelector("#root"));
