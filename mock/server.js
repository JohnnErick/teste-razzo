const express = require("express");
const cors = require("cors");

const app = express();

app.use(
  cors({
    credentials: true,
    origin: (origin, callback) => callback(null, true)
  })
);

app.get("/", (req, res) => res.send("Server Up"));

app.use(express.static("public"));
app.listen(3000, () => console.log("Example app listening on port 3000!"));
